﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnit
{
    [TestFixture]
    class MyTestCase
    {
        [TestCase]
        public void Add()
        {
            MyMath math = new MyMath();
            Assert.AreEqual(31, math.Add(20, 11));
        }

        [TestCase]
        public void Sub()
        {
            MyMath math = new MyMath();
            Assert.AreEqual(10, math.Sub(20, 10));
        }

        [TestCase]
        public void Div()
        {
            MyMath math = new MyMath();
            Assert.AreEqual(5, math.Div(25, 5));
        }

        [TestCase]
        public void Mul()
        {
            MyMath math = new MyMath();
            Assert.AreEqual(15, math.Mul(3, 5));
        }

        [TestCase]
        public void Rem()
        {
            MyMath math = new MyMath();
            Assert.AreEqual(1, math.Rem(5, 2));
        }
    }
}
